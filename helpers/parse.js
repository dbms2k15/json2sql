const fs = require('fs');
const _ = require('lodash');
const config = require('../config');
const Database = require('./Database')
const database = new Database(config.db);

const firstLevel = (obj) => {
  return new Promise((res, rej) => {
    aKeys = Object.keys(obj).filter(k => typeof(obj[k]) !== 'object')
    aValues = aKeys.map(k => {
      let val = _.replace(`${obj[k]}`, /\"/g, "");
      val = val.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
      return `"${val}"`;
    })
    res({aKeys, aValues})
  })
}

const secondLevel2 = (obj) => {
  return new Promise((res, rej) => {
    aKeys = Object.keys(obj).filter(k => typeof(obj[k]) === 'object' && k != 'persona_crew');
    aValues = aKeys.map(l => {
      let temp1 = obj[l];
      return temp1.map(k => {
        let val = _.replace(`${k}`, /\"/g, "");
        val = val.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
        return `"${val}"`;
      })
    })
    res({aKeys, aValues})
  })    
}

const secondLevel = (obj) => {
  return new Promise((res, rej) => {
    const aKeys = Object.keys(obj).filter(k => typeof(obj[k]) === 'object' && k != 'persona_crew');
    res(aKeys);
  })    
}
const getSql = async (keys, anime) => {
  //console.log(keys);
  let sqls = []
  await Promise.all(keys.map(async (key) => {
      let val = anime[key].map(async e => {
      let otherVal = `"${e.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '')}"`;
      let str = `INSERT INTO Anime_${key}(anime_id, ${key}) VALUES (${anime['anime_id']},${otherVal});`
      database.query(str).catch(err => {});
      //console.log(str);
    });
  }))
}


const getPerson = async (anime) => {
  let pckey = 'persona_crew'
  if (_.includes(Object.keys(anime), pckey)) {
    await Promise.all(anime[pckey].map(async (pcEach) => {
        let str2 = ''
        let str1 = `INSERT INTO Anime_persona(anime_id, persona_id) VALUES (${anime['anime_id']},${pcEach['persona']});`
        if ('crew' in pcEach) {
          str2 = `INSERT INTO Voice_actor(persona_id, crew_id) VALUES (${pcEach['persona']},${pcEach['crew']});`
          // database.query(str).catch(err => {});
        }
        let str = `${str1}${str2}`;
        // console.log(str);
        database.query(str).catch(err => {});
    }));
  }
}

const insertL1 = (files) => { 
  for (let i = 0; i < files.length; i++) {
    let contents = fs.readFileSync(`../anime_data/${files[i]}`, 'utf8');

    let anime = JSON.parse(contents);

    firstLevel(anime).then((ret) => {
      let str = `INSERT INTO Anime(${ret.aKeys.join(',')}) VALUES (${ret.aValues.join(',')})`
      return new Promise((res, rej) => {
        res(str)
      })
    }).then((que)=> {
      //console.log(que);
      return database.query(que)
    }).then(rows => {
      //console.log(rows);
    }); 
  }
  console.log('All done');
}


const insertL2 = (files) => {
  for (let i = 0; i < files.length; i++) {
    let contents = fs.readFileSync(`../anime_data/${files[i]}`, 'utf8');
    let anime = JSON.parse(contents);
    secondLevel(anime)
      .then(val => {
        getSql(val, anime);
      })
  }
  console.log('All done');
}

const insertPerson = async (files) => {
  for (let i = 0; i < files.length; i++) {
    let contents = fs.readFileSync(`../anime_data/${files[i]}`, 'utf8');
    let anime = JSON.parse(contents);
    database.query('DELETE FROM Anime_persona;DELETE FROM Voice_actor;')
    .then(() => getPerson(anime));
  }
  console.log('All done');
}

const getPersona = (obj) => {
  return new Promise((res, rej) => {
    aKeys = Object.keys(obj)
    aValues = aKeys.map(k => {
      let ins = obj[k]
      if (typeof(obj[k]) === 'object') {
        ins = _.join(obj[k], '\n')
      }
      let val = _.replace(ins, /\"/g, "'");
      val = val.replace(/[^A-Za-z 0-9\n \.,\?"'!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
      return `"${val}"`;
    })
    for (i =0; i < aKeys.length; i ++) {
      if (aKeys[i] == 'full_name') {
        aKeys[i] = 'primary_name';
      }
    }
    res({aKeys, aValues})
  })
}

const insertPersona = async (files) => {
  // database.query('DELETE FROM Persona;')
  for (let i = 40000; i < files.length; i++) {
    let contents = fs.readFileSync(`../persona3_data/${files[i]}`, 'utf8');
    let persona = JSON.parse(contents);
    
    getPersona(persona)
    .then((ret) => {
      let str = `INSERT INTO Persona(${ret.aKeys.join(',')}) VALUES (${ret.aValues.join(',')})`
      return new Promise((res, rej) => {
        res(str)
      })
    }).then(que => {
      //console.log(que);
      return database.query(que)
    }).catch((err) => {
      //console.log(err);
    })
  }
  console.log('All done');
}

const insertCrew = async (files) => {
  // database.query('DELETE FROM Persona;')
  for (let i = 0; i < 20000; i++) { // files.length
    let contents = fs.readFileSync(`../crew_data/${files[i]}`, 'utf8');
    let persona = JSON.parse(contents);
    
    getPersona(persona)
    .then((ret) => {
      let str = `INSERT INTO Crew(${ret.aKeys.join(',')}) VALUES (${ret.aValues.join(',')})`
      return new Promise((res, rej) => {
        res(str)
      })
    }).then(que => {
      //console.log(que);
      return database.query(que)
    }).catch((err) => {
      //console.log('err');
    })
  }
  console.log('All done');
}

module.exports = {
  insertL1, insertL2, insertPerson, insertPersona, insertCrew,
}