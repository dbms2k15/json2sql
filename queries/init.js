module.exports = `SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS Anime;
DROP TABLE IF EXISTS Crew;
DROP TABLE IF EXISTS User;
DROP TABLE IF EXISTS Persona;
DROP TABLE IF EXISTS Anime_persona;
DROP TABLE IF EXISTS Voice_actor;
DROP TABLE IF EXISTS Watch;
DROP TABLE IF EXISTS Anime_alt_name;
DROP TABLE IF EXISTS Anime_genre;
DROP TABLE IF EXISTS Anime_producer;
DROP TABLE IF EXISTS Anime_licensor;
DROP TABLE IF EXISTS Anime_studio;


CREATE TABLE Anime
(
  anime_id INT NOT NULL,
  primary_name VARCHAR(100) NOT NULL,
  type CHAR(10) ,
  num_episodes INT,
  status VARCHAR(30),
  start_date VARCHAR(20),
  end_date VARCHAR(20),
  premiered VARCHAR(20),
  broadcast VARCHAR(30),
  source CHAR(30),
  duration_mins INT,
  rating VARCHAR(50),
  synopsis TEXT,
  pic_url VARCHAR(100),
  PRIMARY KEY (anime_id)
);

CREATE TABLE Crew
(
  crew_id INT NOT NULL,
  full_name VARCHAR(100) NOT NULL,
  birthday VARCHAR(30),
  website VARCHAR(500),
  more VARCHAR(10000),
  pic_url VARCHAR(200),
  PRIMARY KEY (crew_id)
);

CREATE TABLE User
(
  user_id INT NOT NULL,
  password CHAR(200) NOT NULL,
  gender ENUM ('male', 'female'),
  birthday DATE,
  full_name VARCHAR(100),
  pic_url VARCHAR(100),
  PRIMARY KEY (user_id)
);


CREATE TABLE Persona
(
  persona_id INT NOT NULL,
  full_name VARCHAR(50) NOT NULL,
  pic_url VARCHAR(200),
  details TEXT,
  PRIMARY KEY (persona_id)
);

CREATE TABLE Anime_persona
(
  persona_id INT NOT NULL,
  anime_id INT NOT NULL,
  PRIMARY KEY (persona_id, anime_id),
  FOREIGN KEY (persona_id) REFERENCES Persona(persona_id),
  FOREIGN KEY (anime_id) REFERENCES Anime(anime_id)
);

CREATE TABLE Voice_actor
(
  persona_id INT NOT NULL,
  crew_id INT NOT NULL,
  PRIMARY KEY (crew_id, persona_id),
  FOREIGN KEY (crew_id) REFERENCES Crew (crew_id),
  FOREIGN KEY (persona_id) REFERENCES Persona (persona_id)
);

CREATE TABLE Watch
(
  progress INT,
  score INT,
  status VARCHAR(20),
  user_id INT NOT NULL,
  anime_id INT NOT NULL,
  PRIMARY KEY (user_id, anime_id),
  FOREIGN KEY (user_id) REFERENCES User(user_id),
  FOREIGN KEY (anime_id) REFERENCES Anime(anime_id)
);

CREATE TABLE Anime_alt_name
(
  alt_name VARCHAR(100) NOT NULL,
  anime_id INT NOT NULL,
  PRIMARY KEY (alt_name, anime_id),
  FOREIGN KEY (anime_id) REFERENCES Anime(anime_id)
);

CREATE TABLE Anime_producer
(
  producer VARCHAR(100) NOT NULL,
  anime_id INT NOT NULL,
  PRIMARY KEY (producer, anime_id),
  FOREIGN KEY (anime_id) REFERENCES Anime(anime_id)
);

CREATE TABLE Anime_licensor
(
  licensor VARCHAR(100) NOT NULL,
  anime_id INT NOT NULL,
  PRIMARY KEY (licensor, anime_id),
  FOREIGN KEY (anime_id) REFERENCES Anime(anime_id)
);

CREATE TABLE Anime_studio
(
  studio VARCHAR(100) NOT NULL,
  anime_id INT NOT NULL,
  PRIMARY KEY (studio, anime_id),
  FOREIGN KEY (anime_id) REFERENCES Anime(anime_id)
);

CREATE TABLE Anime_genre
(
  genre VARCHAR(20) NOT NULL,
  anime_id INT NOT NULL,
  PRIMARY KEY (genre, anime_id),
  FOREIGN KEY (anime_id) REFERENCES Anime(anime_id)
);`